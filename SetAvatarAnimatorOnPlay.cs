// PURPOSE:
//  (Intended for VRChat SDK3 avatar scenes.)
//  Unity has a bug where sometimes messing with animator stuff
//  on an avatar in Edit Mode causes the avatar to get permanently
//  stuck in bicycle-pose. It's incredibly frustrating. The best
//  workaround is to keep your avatar animator controller unset
//  in Edit Mode, and then when you want to do animator stuff,
//  enter Play Mode and set the animator controller.
//  However, it's really annoying to have to set the controller
//  every time you enter Play Mode. This script just automates
//  that task! Every time you enter Play Mode, this script will
//  set the avatar's animator controller to the one you set in
//  the script properties.
//
// USAGE:
//  Add this script to your assets.
//  Drag the script to some Game Object in your scene. (The object
//  should be outside the avatar object tree, otherwise the avatar
//  upload window will yell at you for using a custom script.)
//  Then, follow the instructions in the script's Inspector pane.
//
// AUTHOR: aurycat
// LICENSE: MIT
// VERSION: 1.0 (2022-05-14)

#if UNITY_EDITOR
using System;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using VRC.SDK3.Avatars.Components;

public class SetAvatarAnimatorOnPlay : MonoBehaviour
{
    // Other than Disable/None/Custom, must match names in VRCAvatarDescriptor.AnimLayerType
    public enum PlayableLayerSelection {
        Disable,
        None,
        Custom,
        Base,
        Additive,
        Gesture,
        Action,
        FX,
        Sitting,
        TPose,
        IKPose
    }

    [Header("Hover over property names for instructions. You can use multiple", order=0), Space(-10, order=1)]
    [Header("of this script on one object if you have multiple avatars in the scene.", order=2)]
    [Space(10, order=3)]

    [Tooltip("Sets this avatar's main animator controller when the Unity editor enters Play Mode (or when this script gets enabled).")]
    public VRCAvatarDescriptor avatar;
    [Tooltip("The controller to apply to the avatar when entering Play Mode.   'Disable' makes this script do nothing.   'None' removes the currently-set controller.   'Custom' uses the controller from the 'custom' property.   The other options use the controllers set in the avatar's playable layers.")]
    public PlayableLayerSelection controller;
    [Tooltip("If 'controller' is set to 'Custom', use this value instead any of the animators in the avatar's playable layer.")]
    public AnimatorController custom;

    void OnEnable() {
        if (avatar == null || controller == PlayableLayerSelection.Disable) {
            return;
        }

        Animator animator = (Animator)avatar.gameObject.GetComponent(typeof(Animator));
        if (animator == null) {
            Debug.Log("SetAnimatorOnPlay(" + avatar.gameObject.name + "): No animator");
            return;
        }

        if (controller == PlayableLayerSelection.None) {
            animator.runtimeAnimatorController = null;
            return;
        } else if (controller == PlayableLayerSelection.Custom) {
            animator.runtimeAnimatorController = custom;
            return;
        }

        SerializedObject so = new SerializedObject(avatar);
        SerializedProperty baseList = so.FindProperty("baseAnimationLayers");
        SerializedProperty specialList = so.FindProperty("specialAnimationLayers");
        if (!CheckLayerList(animator, baseList)) {
            CheckLayerList(animator, specialList);
        }
    }

    private bool CheckLayerList(Animator animator, SerializedProperty list) {
        for (int i = 0; i < list.arraySize; i++) {
            SerializedProperty layer = list.GetArrayElementAtIndex(i);
            SerializedProperty layerType = layer.FindPropertyRelative("type");
            AnimatorController layerController = (AnimatorController)layer.FindPropertyRelative("animatorController").objectReferenceValue;

            String layerTypeName = System.Enum.GetName(typeof(VRCAvatarDescriptor.AnimLayerType), layerType.enumValueIndex);
            String selectedTypeName = System.Enum.GetName(typeof(PlayableLayerSelection), controller);

            if (layerTypeName == selectedTypeName) {
                animator.runtimeAnimatorController = layerController;
                return true;
            }
        }
        return false;
    }
}
#endif