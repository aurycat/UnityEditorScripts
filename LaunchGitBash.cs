// Menu item to open the current project folder in Git Bash. (Windows Only)
//
// AUTHOR: aurycat
// LICENSE: MIT
// HISTORY:
//  1.0 (2022-06-14)

#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Diagnostics;

namespace Aurycat.Git {

[InitializeOnLoadAttribute]
public static class Git
{
    [MenuItem("Tools/Launch Git Bash")]
    private static void LaunchGitBash() {
        ProcessStartInfo startInfo = new ProcessStartInfo();
        startInfo.FileName = "C:\\Program Files\\Git\\git-bash.exe";
        Process.Start(startInfo);
    }
}

}
#endif