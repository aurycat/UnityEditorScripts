# Aurycat's Unity Editor Scripts (mostly for use in VRChat development)

## Installing

Each .cs file is a self-contained script. You can just download it and copy it into your Assets folder.

Alternatively, they can be downloaded as Unitypackages here:
https://drive.proton.me/urls/PRSAPTFVQG#lDms36qUjFlE

## Summaries

Each script has a description and usage instructions written at the top of the file. Please read those for more information.

* **AutoAnimatorMerger** - Merges animator controllers. Useful for applying prefabs to your existing avatar controllers.
* **ImprovedColliderEditor** - Adds drag handles for the center of Box and Sphere Collider components. Makes working with multiple of the same type of a collider on a single object easier by adding identifying numbers in the scene view when multiple colliders are selected.
* **LaunchGitBash** - Adds a menu item to open the current project folder in Git Bash.
* **LivePropertyRecorder** - Makes it easy to record changes of an object in play mode into an anim file. For example, you can use this to bake Physbone physics into an anim.
* **SetAvatarAnimatorOnPlay** - Sets the animator controller on a VRChat avatar upon entering play mode. Helps workaround a Unity bug where avatars get permanently stuck in bicycle pose.
* **SwitchToSceneViewOnPlay** - The name says it all.

## Etc

All scripts MIT license. Contact me at Aury#8713 for questions or bug reports. Thanks :)