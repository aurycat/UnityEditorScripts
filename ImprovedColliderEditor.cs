// Unity Editor script to improve the Inspector for collider components.
// Currently only works on BoxColliders and SphereColliders.
//
// * Adds colored lables to each collider Inspector component and the scene
//   view so you can easily tell which component matches with which collider
//   when multiple colliders are on one GameObject.
//   (Colors are random, derived from component instance ID.)
// * Adds toggleable center movement handles (code from TechAnon, thanks!)
// * Adds toggleable "highlighting"/face colors to make it easier to identify
//   the position and size of the collider.
// * Ctrl-click on the toggles to turn them on/off for all visible colliders!
// * Might semi-inadvertently fix a bug where Unity's Inspector breaks
//   real bad when multiple BoxColliders are on an object at once.
//
// ----------------------------------------------------------------------------
//
// AUTHOR: aurycat
// LICENSE: MIT
// HISTORY:
//  1.0 (2022-11-13)

#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;

namespace Aurycat.ImprovedColliderEditor {

public class CustomColliderInspectorBase : Editor
{
    protected Type defaultEditorType;
    protected Editor defaultEditor; // Unity's built-in editor
    protected bool supportsHighlight = true;

    protected bool showIDs;

    static public bool allShowHandle = false;
    static public bool allUnshowHandle = false;
    protected bool showHandle;

    static public bool allHighlight = false;
    static public bool allUnhighlight = false;
    protected bool highlight;

    // Using this hack by Cobo3 to extend a default Unity inspector
    // intead of fully replacing it:
    // https://forum.unity.com/threads/extending-instead-of-replacing-built-in-inspectors.407612/

    protected virtual void OnEnable()
    {
        allHighlight = false;
        allUnhighlight = false;
        allShowHandle = false;
        allUnshowHandle = false;
        if (defaultEditorType != null) {
            defaultEditor = Editor.CreateEditor(targets, defaultEditorType);
        }
    }

    protected virtual void OnDisable()
    {
        allHighlight = false;
        allUnhighlight = false;
        allShowHandle = false;
        allUnshowHandle = false;
        if (defaultEditor != null) {
            // When OnDisable is called, the default editor we created should be destroyed to avoid memory leakage.
            // Also, make sure to call any required methods like OnDisable
            MethodInfo disableMethod = defaultEditor.GetType().GetMethod("OnDisable", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            if (disableMethod != null)
                disableMethod.Invoke(defaultEditor, null);
            DestroyImmediate(defaultEditor);
        }
    }

    public override void OnInspectorGUI()
    {
        showIDs = targets.Length != 1;
        if (!showIDs) {
            foreach(Collider _target in targets) {
                if (_target.GetComponents<Collider>().Length > 1) {
                    showIDs = true;
                    break;
                }
            }
        }

        if (showIDs) {
            foreach(Collider _target in targets) {
                var _color = GetColors(_target);
                GUIStyle style = BoldColor(_color.primary);
                string text = "ID: " + Math.Abs(_target.GetInstanceID()).ToString();
                if (targets.Length != 1) {
                    text += " (" + _target.name + ")";
                }
                EditorGUILayout.LabelField(text, style);
            }
        }

        if (defaultEditor != null) {
            defaultEditor.OnInspectorGUI();
        } else {
            DrawDefaultInspector();
        }

        // Modified from TechAnon
        {
            if (allShowHandle) {
                showHandle = true;
            } else if (allUnshowHandle) {
                showHandle = false;
            }

            bool _showHandle = EditorGUILayout.Toggle(
                new GUIContent(
                    "Show Move Handle",
                    "Shows a move transform tool for moving the collider's center. " +
                    "Ctrl-click to show/hide move tool for all visible colliders at once."),
                showHandle);
            if (_showHandle != showHandle) {
                allShowHandle = false;
                allUnshowHandle = false;
                if (EditorGUI.actionKey) {
                    if (_showHandle) {
                        allShowHandle = true;
                    } else {
                        allUnshowHandle = true;
                    }
                }

                EditorWindow.GetWindow<SceneView>().Repaint();
                showHandle = _showHandle;
            }
        }

        if (supportsHighlight) {
            if (allHighlight) {
                highlight = true;
            } else if (allUnhighlight) {
                highlight = false;
            }

            bool _highlight = EditorGUILayout.Toggle(
                new GUIContent(
                    "Highlight",
                    "Highlights the collider with a solid color in the Scene view. " +
                    "Ctrl-click to highlight/unhighlight all visible colliders at once."),
                highlight);
            if (_highlight != highlight) {
                allHighlight = false;
                allUnhighlight = false;
                if (EditorGUI.actionKey) {
                    if (_highlight) {
                        allHighlight = true;
                    } else {
                        allUnhighlight = true;
                    }
                }

                EditorWindow.GetWindow<SceneView>().Repaint();
                highlight = _highlight;
            }
        }
    }

    protected void SceneDrawID(Vector3 position, Color color)
    {
        if (showIDs) {
            GUIStyle style = BoldColor(color);
            Handles.Label(position, Math.Abs(target.GetInstanceID()).ToString(), style);
        }
    }

    protected static Color[] colorArray = {
        Color.black,
        Color.blue,
        Color.cyan,
        Color.green,
        Color.magenta,
        Color.red,
        Color.white,
        Color.yellow,
        new Color(1.0f, 0.647f, 0.0f, 1.0f), // orange
        new Color(0.59f, 0.16f, 1.0f, 1.0f)  // purple
    };

    protected (Color primary, Color transparent) GetColors(Collider c) {
        if (!(showIDs || highlight)) {
            // Not sure how expensive Random is, but this gets called ~every
            // frame so just skip generating colors if we won't be using them.
            return (Color.black, Color.black);
        }

        int id = c.GetInstanceID();

        System.Random r = new System.Random(id);
        Color primary = colorArray[r.Next() % 10];
        Color transparent = primary;

        if (primary == Color.black) {
            transparent *= 0.4f;
        } else {
            transparent *= new Color(0.5f, 0.5f, 0.5f, 0.1f);
        }

        if (!c.enabled) {
            primary.a *= 0.3f;
            transparent.a *= 0.5f;
        }

        return (primary, transparent);
    }

    protected GUIStyle BoldColor(Color c) {
        GUIStyle style = new GUIStyle(EditorStyles.boldLabel);
        style.normal.textColor = c;
        return style;
    }
}


[CustomEditor(typeof(BoxCollider), true)]
[CanEditMultipleObjects]
public class CustomBoxColliderInspector : CustomColliderInspectorBase
{
    protected override void OnEnable() {
        defaultEditorType = Type.GetType("UnityEditor.BoxColliderEditor, UnityEditor");
        base.OnEnable();
    }

    public void OnSceneGUI()
    {
        BoxCollider collider = target as BoxCollider;
        Vector3 cen = collider.center;
        Vector3 worldCenter = collider.transform.TransformPoint(cen);
        var _color = GetColors(collider);

        SceneDrawID(worldCenter, _color.primary);

        if (highlight) {
            //   B----------A
            //    |\       | \
            //    | C----------D
            //    |  |     |  |
            //   F|__|____E|  |
            //    \  |     \  |
            //     \ |      \ |
            //     G\----------H

            Vector3 r = collider.size/2;
            Vector3 A = cen + new Vector3(+r.x,+r.y,+r.z);
            Vector3 B = cen + new Vector3(-r.x,+r.y,+r.z);
            Vector3 C = cen + new Vector3(-r.x,+r.y,-r.z);
            Vector3 D = cen + new Vector3(+r.x,+r.y,-r.z);
            Vector3 E = cen + new Vector3(+r.x,-r.y,+r.z);
            Vector3 F = cen + new Vector3(-r.x,-r.y,+r.z);
            Vector3 G = cen + new Vector3(-r.x,-r.y,-r.z);
            Vector3 H = cen + new Vector3(+r.x,-r.y,-r.z);
            A = collider.transform.TransformPoint(A);
            B = collider.transform.TransformPoint(B);
            C = collider.transform.TransformPoint(C);
            D = collider.transform.TransformPoint(D);
            E = collider.transform.TransformPoint(E);
            F = collider.transform.TransformPoint(F);
            G = collider.transform.TransformPoint(G);
            H = collider.transform.TransformPoint(H);

            Vector3[] top = new Vector3[] { A, B, C, D };
            Vector3[] bot = new Vector3[] { E, F, G, H };
            Vector3[] lef = new Vector3[] { B, C, G, F };
            Vector3[] rig = new Vector3[] { A, D, H, E };
            Vector3[] fro = new Vector3[] { C, D, H, G };
            Vector3[] bak = new Vector3[] { A, E, F, B };

            Color face = _color.transparent;
            Color outline = Color.clear;

            Handles.DrawSolidRectangleWithOutline(top, face, outline);
            Handles.DrawSolidRectangleWithOutline(bot, face, outline);
            Handles.DrawSolidRectangleWithOutline(lef, face, outline);
            Handles.DrawSolidRectangleWithOutline(rig, face, outline);
            Handles.DrawSolidRectangleWithOutline(fro, face, outline);
            Handles.DrawSolidRectangleWithOutline(bak, face, outline);
        }

        // From TechAnon
        if (showHandle) {
            EditorGUI.BeginChangeCheck();
            Vector3 center = collider.center;
            Vector3 extent = Vector3.zero; //collider.size * -0.25f;
            center = collider.transform.TransformPoint(center + extent);
            center = Handles.PositionHandle(center, collider.transform.rotation);
            center = collider.transform.InverseTransformPoint(center) - extent;
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(collider, "Update Boundry Position");
                collider.center = center;
            }
        }
    }
}


[CustomEditor(typeof(SphereCollider), true)]
[CanEditMultipleObjects]
public class CustomSphereColliderInspector : CustomColliderInspectorBase
{
    protected override void OnEnable()
    {
        defaultEditorType = Type.GetType("UnityEditor.SphereColliderEditor, UnityEditor");
        base.OnEnable();
    }

    public void OnSceneGUI()
    {
        SphereCollider collider = target as SphereCollider;
        Vector3 worldCenter = collider.transform.TransformPoint(collider.center);
        var _color = GetColors(collider);

        SceneDrawID(worldCenter, _color.primary);

        if (highlight) {
            Color prev = Handles.color;
            Handles.color = _color.transparent;
            Vector3 normal = collider.transform.TransformDirection(Vector3.up);
            Handles.DrawSolidDisc(worldCenter, normal, collider.radius);
            Handles.color = prev;
        }

        // From TechAnon
        if (showHandle) {
            EditorGUI.BeginChangeCheck();
            Vector3 center = collider.center;
            Vector3 extent = Vector3.zero; //Vector.one * collider.radius * -0.25f;
            center = collider.transform.TransformPoint(center + extent);
            center = Handles.PositionHandle(center, collider.transform.rotation);
            center = collider.transform.InverseTransformPoint(center) - extent;
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(collider, "Update Boundry Position");
                collider.center = center;
            }
        }
    }
}

}

#endif