// Records live game object property changes made during edit mode
// to an .anim file. This can be used to bake physics, record scripted
// material changes, etc.
//
// USAGE:
// 1. Add this script to your assets.
// 2. Add a LivePropertyRecorder component the root of the GameObject
//    tree that you want to record.
// 3. Disable the LivePropertyRecorder component.
// 4. Configure the component settings; set the objects that should
//    be recorded.
// 5. Enter play mode.
// 6. Enable the component when you want to start recording.
// 7. To stop recording and save the result, disable the component
//    or exit play mode.
//
// AUTHOR: aurycat
// LICENSE: MIT
// HISTORY:
//  0.1 (2022-07-10)

#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;

public class LivePropertyRecorder : MonoBehaviour
{
    private const string defaultPath = "Assets/Recording.anim";

    private GameObjectRecorder recorder;
    private bool error = false;
    private bool finished = false;

    [Header("Output Clip Settings")]
    [Tooltip("The animation clip to record into. If this is not set, a default path will be chosen. If the clip set is not empty, a new clip will be created next to it.")]
    public AnimationClip clip;
    [Tooltip("The root of the animation hierarchy (where the Animator component will go). Leave this blank to be the object that this LivePropertyRecorder component is on.")]
    public GameObject root;
    [Tooltip("The FPS of the saved recording.")]
    public float outputFPS = 60;
    [Tooltip("How much to 'compress' together small changes in properties to save space and performance. 0 is no compression, 100 is max. Unity recommends 0.5 by default.")]
    public float keyframeCompression = 0.5f;

    [Header("Objects to Record (Single)")]
    [Tooltip("These objects will have all their properties recorded.")]
    public GameObject[] allProperties;
    [Tooltip("These objects will have their GameObject active (enabled) state recoded.")]
    public GameObject[] activeState;
    [Tooltip("These objects will have their transforms recorded.")]
    public GameObject[] transforms;
    [Tooltip("These objects will have their meshes and all the material properties recorded.")]
    public GameObject[] meshesAndMaterials;

    [Header("Objects to Record (Including Children)")]
    [Tooltip("These objects, and their children, will have all their properties recorded.")]
    public GameObject[] allPropertiesRecursive;
    [Tooltip("These objects, and their children, will have their transforms recorded.")]
    public GameObject[] transformsRecursive;
    [Tooltip("These objects, and their children, will have their meshes and all the material properties recorded.")]
    public GameObject[] meshesAndMaterialsRecursive;


    void Start()
    {
        // Create an empty clip to record to
        if (clip == null || !clip.empty) {
            string path = null;
            if (clip != null) {
                path = AssetDatabase.GetAssetPath(clip);
            }
            if (path == null || path == "") {
                path = defaultPath;
            }

            path = AssetDatabase.GenerateUniqueAssetPath(path);
            clip = new AnimationClip();
            AssetDatabase.CreateAsset(clip, path);
        }

        // Create recorder
        if (root == null) {
            root = gameObject;
        }
        recorder = new GameObjectRecorder(root);
        error = false;

        // Set properties to record
        foreach (GameObject obj in allProperties) {
            if (obj == null || !CheckChild(obj, "All Properties")) { continue; }
            recorder.BindAll(obj, false);
        }
        foreach (GameObject obj in activeState) {
            if (obj == null || !CheckChild(obj, "Active State")) { continue; }
            string path = AnimationUtility.CalculateTransformPath(obj.transform, root.transform);
            EditorCurveBinding binding = EditorCurveBinding.DiscreteCurve(path, typeof(GameObject), "m_IsActive");
            recorder.Bind(binding);
        }
        foreach (GameObject obj in transforms) {
            if (obj == null || !CheckChild(obj, "Transforms")) { continue; }
            recorder.BindComponentsOfType<Transform>(obj, false);
        }
        foreach (GameObject obj in meshesAndMaterials) {
            if (obj == null || !CheckChild(obj, "Meshes and Materials")) { continue; }
            recorder.BindComponentsOfType<MeshRenderer>(obj, false);
            recorder.BindComponentsOfType<SkinnedMeshRenderer>(obj, false);
        }
        foreach (GameObject obj in allPropertiesRecursive) {
            if (obj == null || !CheckChild(obj, "All Properties Recursive")) { continue; }
            recorder.BindAll(obj, true);
        }
        foreach (GameObject obj in transformsRecursive) {
            if (obj == null || !CheckChild(obj, "Transforms Recursive")) { continue; }
            recorder.BindComponentsOfType<Transform>(obj, true);
        }
        foreach (GameObject obj in meshesAndMaterialsRecursive) {
            if (obj == null || !CheckChild(obj, "Meshes and Materials Recursive")) { continue; }
            recorder.BindComponentsOfType<MeshRenderer>(obj, true);
            recorder.BindComponentsOfType<SkinnedMeshRenderer>(obj, true);
        }
    }

    void OnEnable() {
        if (error) {
            Debug.LogError("[LivePropertyRecorder] Please exit play mode, fix errors, and then re-enter play mode.");
            enabled = false;
        } else if (finished) {
            Debug.LogError("[LivePropertyRecorder] Please exit and re-enter play mode to record again.");
            enabled = false;
        }
    }

    bool CheckChild(GameObject child, string grp) {
        // Make sure the object is a child of the selected `root` object,
        // otherwise the recorded property paths will get wonky.
        if (child != null && child.transform != root.transform && !child.transform.IsChildOf(root.transform)) {
            string childPath = (child.transform == child.transform.root)
                ? child.name
                : AnimationUtility.CalculateTransformPath(child.transform, child.transform.root);
            Debug.LogError($"[LivePropertyRecorder] Object '{childPath}' in '{grp}' group is not a child of the root object.");
            error = true;
            enabled = false;
            return false;
        }
        return true;
    }

    void Update()
    {
        if (clip == null || error || finished)
            return;

        recorder.TakeSnapshot(Time.deltaTime);
    }

    void OnDisable()
    {
        if (clip == null || error || finished)
            return;

        if (recorder.isRecording)
        {
            finished = true;
            CurveFilterOptions opts;
            opts.keyframeReduction = (keyframeCompression > 0);
            opts.floatError = keyframeCompression;
            opts.positionError = keyframeCompression;
            opts.rotationError = keyframeCompression;
            opts.scaleError = keyframeCompression;

            recorder.SaveToClip(clip, outputFPS, opts);
            AssetDatabase.SaveAssets();
        }
    }
}
#endif